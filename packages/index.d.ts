/**
 * JS对象
 */
interface IObject {
  [key: string]: string | number;
}
/**
 * 允许转换的视口单位
 */
type IUnit = 'vw' | 'vmin' | 'vmax';
/**
 * 参数配置
 */
interface IOptions {
  /**
   * 设计稿宽度
   * @default 750
   */
  viewportWidth: number;
  /**
   * 转换后的视口单位
   * @default "vw"
   */
  viewportUnit: IUnit;
  /**
   * 转换后的字体视口单位
   * @default "vw"
   */
  fontViewportUnit: IUnit;
  /**
   * 单位精度
   * @default 10
   */
  unitPrecision: number;
}
/**
 * 可选参数配置
 */
type IPartialOptions = Partial<IOptions>;
/**
 * 初始化配置
 */
type IInitUnit = (options?: IPartialOptions) => void;
/**
 * 单位转换
 */
type ITransform = (value: number) => number;
/**
 * 书写行内样式
 */
type IInlineStyles = (styles: IObject, options?: IPartialOptions) => IObject;
/**
 * 初始化配置
 */
export declare const initUnit: IInitUnit;
/**
 * 单位转换：px to rpx
 */
export declare const px2rpx: ITransform;
/**
 * 单位转换：rpx to px
 */
export declare const rpx2px: ITransform;
/**
 * 书写行内样式
 */
export declare const inlineStyles: IInlineStyles;
declare const _default: {
  /**
   * 初始化配置
   */
  initUnit: IInitUnit;
  /**
   * 单位转换：px to rpx
   */
  px2rpx: ITransform;
  /**
   * 单位转换：rpx to px
   */
  rpx2px: ITransform;
  /**
   * 书写行内样式
   */
  inlineStyles: IInlineStyles;
};
export default _default;

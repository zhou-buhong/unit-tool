# unit-tool

#### 介绍

一个单位转换工具，可以在内联样式里使用 rpx 作为单位，自动转换为 'vw' | 'vmin' | 'vmax' 三选一，也可以使用 px2rpx 或
rpx2px 进行单位转换。

#### 安装

```bash
$ npm i unit-tool

$ pnpm i unit-tool

$ yarn add unit-tool
```

#### 推荐使用的 node 版本和 npm 版本

```json
{
  "node": ">=14.x.x",
  "npm": ">=6.x.x"
}
```

#### 使用教程

```jsx
import { initUnit, px2rpx, rpx2px, inlineStyles } from 'unit-tool';

// 有默认值，也可以根据自己的设计稿尺寸配置
initUnit({
  viewportWidth: 750,
  viewportUnit: 'vw',
  fontViewportUnit: 'vw',
  unitPrecision: 2,
}); // no return

// 假设现在手机屏幕宽度是375
px2rpx(100); // return 200
rpx2px(100); // return 50

// 可以直接写内联样式
inlineStyles({
  width: '100rpx',
  height: '100rpx',
  backgroundColor: '#ff0000',
}); // return { width: '13.33vw', height: '13.33vw', backgroundColor: '#ff0000' }

export default function Demo() {
  const style = {
    width: '100rpx',
    height: '100rpx',
    backgroundColor: '#ff0000',
  };

  return (
    <div id="unit-tool-demo" style={inlineStyles(style)}>
      DEMO
    </div>
  );
}
```

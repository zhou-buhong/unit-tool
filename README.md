#### 下载依赖（无 package-lock.json）

```bash
$ npm run i
```

#### 下载依赖（有 package-lock.json）

```bash
$ npm run ci
```

#### 启动调试

```bash
$ npm run dev
```

#### 更新依赖包和依赖包锁

```bash
$ npm run up
```

#### 打包

```bash
$ npm run build
```

#### 发布 npm 包

```bash
$ cd packages

$ npm run push

// MacBook
$ cd ..

// Windows
$ cd..
```

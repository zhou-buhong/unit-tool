import { initUnit, inlineStyles, rpx2px, px2rpx } from './packages';

initUnit({ unitPrecision: 2 });

console.log(
  'inlineStyles: ',
  inlineStyles({
    width: '100rpx',
    height: 'calc(200rpx - 100rpx)',
    backgroundColor: '#ff0000',
  }),
);

console.log('rpx2px: ', rpx2px(100));

console.log('rpx2px: ', px2rpx(100));
